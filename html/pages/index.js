$(document).ready(function () {

    getMonthDifference = function (startDate, endDate) {
        var one_day=1000*60*60*24;

        var difference_ms = endDate - startDate;
        return Math.round((difference_ms/one_day)/30);
    };

    // Fetch Course List
    fetchList = function () {
        var displayResources = $('#course-list');
        var displayFeaturedCourses = $('#top-courses');

        displayResources.text('Loading data...');
        displayFeaturedCourses.text('Loading data...');

        $.ajax({
            type: "GET",
            url: app.address+"/api/course",
            success: function(courses)
            {
                console.log(courses);
                var featuredCourses = "";

                var output= "<table class=\"table table-hover course-list-table tablesorter\">" +
                    "<thead>\n" +
                    "<tr>\n" +
                    "<th>Course Id</th>\n" +
                    "<th>Course Name</th>\n" +
                    "<th>Course Starts</th>\n" +
                    "<th>Course Ends</th>\n" +
                    "<th>Location</th>\n" +
                    "<th>Price</th>\n" +
                    "<th>Instructor</th>\n" +
                    "<th>Register</th>\n" +
                    "</tr>\n" +
                    "</thead>" +
                    "<tbody>";

                for (var course in courses)
                {
                    var startDate = new Date(courses[course].startDate.year, courses[course].startDate.monthValue, courses[course].startDate.dayOfMonth, courses[course].startDate.hour, courses[course].startDate.minute);
                    var endDate = new Date(courses[course].endDate.year, courses[course].endDate.monthValue, courses[course].endDate.dayOfMonth, courses[course].endDate.hour, courses[course].endDate.minute);

                    if (course < 4)
                    {
                        featuredCourses += "<div class=\"col-md-3 col-sm-3\">\n" +
                            "<article class=\"featured-course\">\n" +
                            "<figure class=\"image\">\n" +
                            "<div class=\"image-wrapper\"><a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\"><img src=\"assets/img/course-01.jpg\"></a></div>\n" +
                            "</figure>\n" +
                            "<div class=\"description\">\n" +
                            "<a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\"><h3>"+ courses[course].courseTitle +"</h3></a>\n" +
                            "<a class=\"course-category\">"+ courses[course].courseCategory +"</a>\n" +
                            "<hr>\n" +
                            "<div class=\"course-meta\">\n" +
                            "<span class=\"course-date\"><i class=\"fa fa-calendar-o\"></i>" + courses[course].startDate.monthValue + "-" + courses[course].startDate.dayOfMonth + "-" + courses[course].startDate.year +"</span>\n" +
                            "<span class=\"course-length\"><i class=\"fa fa-clock-o\"></i>" + getMonthDifference(startDate, endDate) + " months </span>" +
                            "</div>\n" +
                            "<div class=\"stick-to-bottom\"><a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\" class=\"btn btn-framed btn-color-grey btn-small\">View Details</a></div>\n" +
                            "</div>\n" +
                            "</article><!-- /.featured-course -->\n" +
                            "</div><!-- /.col-md-3 -->\n";
                    }

                    var coursePrice = '';
                    var courseCurrency = '';
                    for (var courseFee in courses[course].courseFeeList)
                    {
                        if (courses[course].courseFeeList[courseFee].feeType == "Course Fee")
                        {
                            coursePrice = courses[course].courseFeeList[courseFee].fee;
                            courseCurrency = courses[course].courseFeeList[courseFee].feeCurrency;
                        }
                    }

                    output+=" <tr><td  class=\"course-title\"> <a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\">" +
                        courses[course].courseId + "</a></td><td>" +
                        courses[course].courseTitle + "</td><td>" +
                        startDate.toLocaleString().replace(new RegExp('/', 'g'),'-') +"</td><td>" +
                        endDate.toLocaleString().replace(new RegExp('/', 'g'),'-') +"</td><td>" +
                        courses[course].courseLocation + "</td><td>" +
                        courseCurrency + " " +coursePrice + "</td><td>" +
                        courses[course].courseInstructor + "</td><td>" +
                        " <a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\"> Register </a>" + "</td><td>";
                }
                output+="</tbody></table>";

                displayResources.html(output);
                displayFeaturedCourses.html(featuredCourses);
            }
        });
    };

    fetchList();
});