var courseData = '';
var formData = '';
var userName = '';
var userEmail = '';

$(document).ready(function () {

    var courseIdParam = '';

    urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results != null)
        {
            return results[1]
        }
    };

    // Fetch Course
    fetchCourse = function () {
        var displayCourseInfo = $('#course-info');
        displayCourseInfo.text('Loading data...');

         courseIdParam = urlParam("courseId");
         if (courseIdParam != null && courseIdParam != '')
         {
             $.ajax({
                 type: "GET",
                 url: app.address+"/api/course?courseId="+courseIdParam,
                 success: function(course)
                 {
                     courseData = course[0];

                     var coursePrice = '';
                     var courseCurrency = '';
                     for (var courseFee in courseData.courseFeeList)
                     {
                         if (courseData.courseFeeList[courseFee].feeType == "Course Fee")
                         {
                             coursePrice = courseData.courseFeeList[courseFee].fee;
                             courseCurrency = courseData.courseFeeList[courseFee].feeCurrency;
                         }
                     }

                     courseData.coursePrice = coursePrice;
                     courseData.courseCurrency = courseCurrency;

                     //make course info string
                     var courseInfoText= "";
                     courseInfoText += "<address>\n" +
                         "<h3>"+course[0].courseTitle+"</h3>\n" +
                         "</address>\n" +
                         "<hr>\n" +
                         "<p>\n" + course[0].courseDescription+
                         "</p>";

                     displayCourseInfo.html(courseInfoText);
                 }
             });
         }
    };
    fetchCourse();

    sendConfirmationEmail = function () {
        event.preventDefault();
        var name  = userName;
        var email  = app.email+","+userEmail;
        var message  = "User: "+userName+" "+"Registered for the course: "+courseData.courseId;

        var userEmailData = {
            name : name,
            subject: "Course Registration Email",
            emailAddress : email,
            query : message
        };

        $.ajax({
            type: "POST",
            url: app.address+"/api/email",
            data: JSON.stringify(userEmailData),
            contentType: 'application/json',

            success: function (response) {
                swal("Great!", "Registration successful", "success");
            },
            error: function (error) {
                swal("Oops!", "Registration failed!", "error");
            }
        });
    };

    var handler = StripeCheckout.configure({
        key: app.stripe,
        ///image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        image: 'assets/img/logo.svg',
        locale: 'auto',
        token: function(token) {

            formData.paymentAmount = courseData.coursePrice * 100;
            formData.paymentCurrency = 'CAD';
            formData.stripeEmail = token.email;
            formData.stripeToken = token.id;

            $.ajax({
                type: "POST",
                url: app.address+"/api/course/register",
                data: JSON.stringify(formData),
                contentType: 'application/json',
                success: function (response) {
                    sendConfirmationEmail();
                },
                error: function (error) {
                    $("#error-message").css('display', 'block');
                    swal("Oops!", "Registration failed!", "error");

                }
            });
        }
    });

    $( "#course-registration-form" ).submit(function( event ) {
        event.preventDefault();
        var firstName  = event.currentTarget['firstName'].value;
        var lastName  = event.currentTarget['lastName'].value;
        var email  = event.currentTarget['email'].value;
        var phoneNumber  = event.currentTarget['phoneNumber'].value;
        var postalCode  = event.currentTarget['postalCode'].value;
        var city  = event.currentTarget['city'].value;
        var address  = event.currentTarget['address'].value;

        userName = firstName + " " + lastName;
        userEmail = email;

        formData = {
            user: {
                firstName : firstName,
                lastName : lastName,
                email: email,
                phoneNumber: phoneNumber,
                postalCode: postalCode,
                city: city,
                address: address
            },
            courseId : courseIdParam,
            paymentStatus : 'unpaid'
        };

        // Open Checkout with further options:
        handler.open({
            name: 'Practicum Canada',
            currency: 'CAD',
            amount: courseData.coursePrice * 100,
            allowRememberMe: 'false'
        });
        event.preventDefault();
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
        handler.close();
    });
});