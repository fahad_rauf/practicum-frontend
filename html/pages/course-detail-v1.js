$(document).ready(function () {

    getWeekDifference = function (startDate, endDate) {
        var one_day=1000*60*60*24;

        var difference_ms = endDate - startDate;
        return Math.round((difference_ms/one_day)/7);
    };

    urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    };

    // Fetch Course List
    fetchList = function () {
        var displayTitle = $('#course-title');
        var displayDateAndCategory = $('#course-date-cat');
        var displayCourseSummary = $('#course-summary');
        var displayCourseBrief = $('#course-brief');
        var displayCourseOutline = $('#course-outline');
        var displayCourseContent = $('#course-content');
        var displayCourseFee = $('#course-fee');
        var displayCourseInstructor = $('#course-instructor');
        var displayCourseDeliverables = $('#course-deliverables');
        var displayCourseCountDown = $('#course-count-down');

        var displayJoinCourse = $('#join-to-course');

        displayTitle.text('Loading data...');
        displayDateAndCategory.text('Loading data...');
        displayCourseSummary.text('Loading data...');
        displayCourseOutline.text('Loading data...');
        displayCourseContent.text('Loading data...');
        displayCourseFee.text('Loading data...');
        displayCourseInstructor.text('Loading data...');
        displayCourseDeliverables.text('Loading data...');
        displayCourseCountDown.text('Loading data...');
        displayJoinCourse.text('Loading data...');

        var courseIdParam = urlParam("courseId");
        $.ajax({
            type: "GET",
            url: app.address+"/api/course?courseId="+courseIdParam,
            success: function(course)
            {
                //get dates
                var startDate = new Date(course[0].startDate.year, course[0].startDate.monthValue, course[0].startDate.dayOfMonth);
                var endDate = new Date(course[0].endDate.year, course[0].endDate.monthValue, course[0].endDate.dayOfMonth);

                //day of week
                var dayOfWeek = course[0].startDate.dayOfWeek.toLowerCase();
                dayOfWeek = dayOfWeek.charAt(0).toUpperCase() + dayOfWeek.slice(1);

                //month
                var month = course[0].startDate.month.toLowerCase();
                month = month.charAt(0).toUpperCase() + month.slice(1);

                //make title string
                var titleText= "";
                titleText += "<h1>"+ course[0].courseTitle+"</h1>";

                //make date and category string
                var dateAndCategoryText= "";
                dateAndCategoryText += "<h2  class=\"course-date\">"+dayOfWeek+" "+ month +" "+ course[0].startDate.dayOfMonth +", "+ course[0].startDate.year+"</h2>"+
                    "<div class=\"course-category\">Category: "+course[0].courseCategory+"</a></div>";

                //course summary text
                var courseSummaryText = "";
                courseSummaryText += "<span class=\"course-summary\" id=\"course-length\"><i class=\"fa fa-calendar-o\"></i>"+getWeekDifference(startDate, endDate)+" weeks</span>";

                //course brief text
                var courseBriefText = "";
                courseBriefText +="<header><h2>Course Brief</h2></header>\n" + "<p>\n" + course[0].courseDescription + "</p>";

                //course instructor text
                var courseInstructorText = "";
                courseInstructorText +="<header><h2>Course Instructor</h2></header>\n" + "<p>\n" + course[0].courseInstructor + "</p>";

                //course deliverable list text
                var courseDeliverableText = "";
                courseDeliverableText +="<header><h2>Course Training Deliverables</h2></header>";
                for (var courseDeliverable in course[0].courseDeliverables)
                {
                    courseDeliverableText +="<p>" + course[0].courseDeliverables[courseDeliverable].deliverable + "</p>";
                }

                //course outline text
                var courseOutlineText = "";
                courseOutlineText +="<h2>Course Outline</h2>" + "<p>" + course[0].courseOutline + "</p>";

                //course content text
                var courseContentText = "";
                courseContentText +="<h2>General Course Content</h2>" +
                    "<p>" + course[0].courseContentInfo + "</p><ul style=\"list-style-type: square;\">";
                for (var courseContent in course[0].courseContents)
                {
                    courseContentText +="<li><b>" + course[0].courseContents[courseContent].courseModule + "</b><ol></br>";
                    for (var subModule in course[0].courseContents[courseContent].subModules)
                    {
                        courseContentText +="<li><p>" + course[0].courseContents[courseContent].subModules[subModule] + "</p></li>";
                    }
                    courseContentText +="</ol></li>";
                }
                courseContentText +="</ul>";
                var coursePrice = '';
                var examPrice = '';
                var pmiExamPrice = '';
                var courseCurrency = '';
                for (var courseFee in course[0].courseFeeList)
                {
                    if (course[0].courseFeeList[courseFee].feeType == "Course Fee")
                    {
                        coursePrice = course[0].courseFeeList[courseFee].fee;
                        courseCurrency = course[0].courseFeeList[courseFee].feeCurrency;
                    }
                    if (course[0].courseFeeList[courseFee].feeType == "Exam Fee")
                    {
                        examPrice = course[0].courseFeeList[courseFee].fee;
                    }
                    if (course[0].courseFeeList[courseFee].feeType == "PMI Members Fee")
                    {
                        pmiExamPrice = course[0].courseFeeList[courseFee].fee;
                    }
                }

                //course fee text
                var courseFeeText = "";
                courseFeeText +="<header><h2>Course & Exam Fee</h2></header>" +
                    "<p>Course Fee is " + courseCurrency + " " + coursePrice + " per participant.</p>" +
                "<p>Exam Fee is " + courseCurrency + " " + examPrice + " ("+courseCurrency+" "+pmiExamPrice+" for PMI members).</p>";

                //count down
                var countDownText = "";
                countDownText += "<a href=\"register-sign-in.html?courseId="+course[0].courseId+"\" class=\"btn\" id=\"btn-course-join\">Join Course</a>";

                displayTitle.html(titleText);
                displayDateAndCategory.html(dateAndCategoryText);
                displayCourseSummary.html(courseSummaryText);
                displayCourseBrief.html(courseBriefText);
                displayCourseFee.html(courseFeeText);
                displayCourseDeliverables.html(courseDeliverableText);
                displayCourseOutline.html(courseOutlineText);
                displayCourseContent.html(courseContentText);
                displayCourseInstructor.html(courseInstructorText);
                displayCourseCountDown.html(countDownText);
                displayJoinCourse.html(countDownText);
            }
        });
    };
    fetchList();
});