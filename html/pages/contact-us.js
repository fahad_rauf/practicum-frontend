$(document).ready(function () {
    $( "#contact-us-form" ).submit(function( event ) {
        event.preventDefault();

        var name  = event.currentTarget['name'].value;
        var email  = event.currentTarget['email'].value;
        var message  = event.currentTarget['message'].value;

        var formData = {
            name : name,
            subject: "Contact Us Request",
            emailAddress : email,
            query : message
        };

        $.ajax({
            type: "POST",
            url: app.address+"/api/contact_us",
            data: JSON.stringify(formData),
            contentType: 'application/json',

            success: function (response) {
                $("#success-message").css('display', 'block');
            },
            error: function (error) {
                $("#error-message").css('display', 'block');
            }
        });
    });
});