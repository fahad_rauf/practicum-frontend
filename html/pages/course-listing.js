$(document).ready(function () {

    // Fetch Course List
    fetchList = function () {
        var displayResources = $('#course-list');
        console.log(displayResources);

        displayResources.text('Loading data from JSON source...');

        $.ajax({
            type: "GET",
            url: app.address+"/api/course",
            success: function(courses)
            {
                console.log(courses);

                var output= "<table class=\"table table-hover course-list-table tablesorter\">" +
                    "<thead>\n" +
                    "<tr>\n" +
                    "<th>Course Id</th>\n" +
                    "<th>Course Name</th>\n" +
                    "<th>Course Starts</th>\n" +
                    "<th>Course Ends</th>\n" +
                    "<th>Location</th>\n" +
                    "<th>Price</th>\n" +
                    "<th>Instructor</th>\n" +
                    "<th>Register</th>\n" +
                    "</tr>\n" +
                    "</thead>" +
                    "<tbody>";

                for (var course in courses)
                {
                    var startDate = new Date(courses[course].startDate.year, courses[course].startDate.monthValue, courses[course].startDate.dayOfMonth, courses[course].startDate.hour, courses[course].startDate.minute);
                    var endDate = new Date(courses[course].endDate.year, courses[course].endDate.monthValue, courses[course].endDate.dayOfMonth, courses[course].endDate.hour, courses[course].endDate.minute);

                    var coursePrice = '';
                    var courseCurrency = '';
                    for (var courseFee in courses[course].courseFeeList)
                    {
                        if (courses[course].courseFeeList[courseFee].feeType == "Course Fee")
                        {
                            coursePrice = courses[course].courseFeeList[courseFee].fee;
                            courseCurrency = courses[course].courseFeeList[courseFee].feeCurrency;
                        }
                    }

                    output+=" <tr><td  class=\"course-title\"> <a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\">" +
                        courses[course].courseId + "</a></td><td>" +
                        courses[course].courseTitle + "</td><td>" +
                        startDate.toLocaleString().replace(new RegExp('/', 'g'),'-') +"</td><td>" +
                        endDate.toLocaleString().replace(new RegExp('/', 'g'),'-') +"</td><td>" +
                        courses[course].courseLocation + "</td><td>" +
                        courseCurrency + " " +coursePrice + "</td><td>" +
                        courses[course].courseInstructor + "</td><td>" +
                        " <a href=\"course-detail-v1.html?courseId="+courses[course].courseId+"\"> Register </a>" + "</td><td>";
                }
                output+="</tbody></table>";

                displayResources.html(output);
            }
        });
    };
    fetchList();
});